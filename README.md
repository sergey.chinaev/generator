# generator

The generator package is designed to service for automatically generate repository files

## Installation

```
go get "gitlab.com/golight/generator"
```

## Description

```go
//declaration of storage
type StorageService struct {
	Storage genstorage.Storage
	FileName string
	StorageTemplate *template.Template
	InterfaceTemplate *template.Template
	TemplateData interface{}
}
```
```go
//creating new storage
func NewStorageService(storage genstorage.Storage, fileName string, storageTemplate *template.Template, interfaceTemplate *template.Template, templateData interface{}) *StorageService

```
```go
//creating templates for storage and interface
func NewStorageTemplate() (*template.Template, error)

func NewInterfaceTemplate() (*template.Template, error)
```
```go
//creating storage files
func (s *StorageService) CreateStorageFiles() error
```
## Usage

```go
//variables
var storage genstorage.Storage
var service generator.StorageService
//creating template of storage
storageTemplate, err := generator.NewStorageTemplate()
if err != nil {
	log.Fatal(err)
}
//creating template of interface
interfaceTemplate, err := generator.NewInterfaceTemplate()
if err != nil {
	log.Fatal(err)
}
//creating new storage
generator.NewStorageService(storage, "file name", storageTemplate, interfaceTemplate, "template data")
//creating storage files
err = service.CreateStorageFiles()
if err != nil {
	log.Fatal(err)
}
```

## Roadmap
- Tests