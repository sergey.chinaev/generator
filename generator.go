package generator

import (
	"fmt"
	"gitlab.com/golight/orm/genstorage"
	"log"
	"os"
	"path/filepath"
	"text/template"
)

type StorageService struct {
	Storage           genstorage.Storage
	FileName          string
	StorageTemplate   *template.Template
	InterfaceTemplate *template.Template
	TemplateData      interface{}
}

func NewStorageService(storage genstorage.Storage, fileName string, storageTemplate *template.Template, interfaceTemplate *template.Template, templateData interface{}) *StorageService {
	return &StorageService{
		Storage:           storage,
		FileName:          fileName,
		StorageTemplate:   storageTemplate,
		InterfaceTemplate: interfaceTemplate,
		TemplateData:      templateData,
	}
}

func NewStorageTemplate() (*template.Template, error) {
	tmpl, err := genstorage.NewStorageTemplate()
	if err != nil {
		return nil, err
	}

	return tmpl, nil
}

func NewInterfaceTemplate() (*template.Template, error) {
	tmpl, err := genstorage.NewInterfaceTemplate()
	if err != nil {
		return nil, err
	}

	return tmpl, nil
}

func (s *StorageService) CreateStorageFiles() error {
	directory := "../repository/"

	err := os.MkdirAll(directory, 0755)
	if err != nil {
		return err
	}

	files := []struct {
		path     string
		template *template.Template
	}{
		{filepath.Join(directory, s.FileName+"_storage.go"), s.StorageTemplate},
		{filepath.Join(directory, s.FileName+"_interface.go"), s.InterfaceTemplate},
	}

	for _, f := range files {
		var file *os.File
		if _, err := os.Stat(f.path); os.IsNotExist(err) {
			file, err = os.Create(f.path)
			if err != nil {
				return err
			}
			defer file.Close()
			log.Printf("File `%s` created", f.path)
		} else {
			return fmt.Errorf("File `%s` already exists", f.path)
		}

		err := f.template.Execute(file, s.TemplateData)
		if err != nil {
			return err
		}
	}

	return nil
}
