package main

import (
	"generator"
	"gitlab.com/golight/orm/genstorage"
	"log"
)

func main() {
	var storage genstorage.Storage
	var service generator.StorageService
	storageTemplate, err := generator.NewStorageTemplate()
	if err != nil {
		log.Fatal(err)
	}
	interfaceTemplate, err := generator.NewInterfaceTemplate()
	if err != nil {
		log.Fatal(err)
	}

	generator.NewStorageService(storage, "file name", storageTemplate, interfaceTemplate, "template data")
	err = service.CreateStorageFiles()
	if err != nil {
		log.Fatal(err)
	}
}
