package generator

import (
	"gitlab.com/golight/orm/genstorage"
	"reflect"
	"testing"
	"text/template"
)

type TestCases struct {
	incomeStorage           genstorage.Storage
	incomeFilename          string
	incomeStorageTemplate   template.Template
	incomeInterfaceTemplate template.Template
	incomeTemplateData      TestData
	expected                StorageService
}
type TestData struct {
	Name string
}

func TestNewStorageService(t *testing.T) {
	tests := []TestCases{
		{
			incomeStorage:           genstorage.Storage{},
			incomeFilename:          "test",
			incomeStorageTemplate:   *template.Must(template.New("storage").Parse("storage template content")),
			incomeInterfaceTemplate: *template.Must(template.New("interface").Parse("interface template content")),
			incomeTemplateData:      TestData{Name: "Test Data"},
			expected: StorageService{
				Storage:           genstorage.Storage{},
				FileName:          "test",
				StorageTemplate:   template.Must(template.New("storage").Parse("storage template content")),
				InterfaceTemplate: template.Must(template.New("interface").Parse("interface template content")),
				TemplateData:      TestData{Name: "Test Data"},
			},
		},
	}

	for _, tc := range tests {
		result := NewStorageService(tc.incomeStorage, tc.incomeFilename, &tc.incomeStorageTemplate, &tc.incomeInterfaceTemplate, tc.incomeTemplateData)

		if !reflect.DeepEqual(*result, tc.expected) || result.StorageTemplate.DefinedTemplates() != tc.expected.StorageTemplate.DefinedTemplates() || result.InterfaceTemplate.DefinedTemplates() != tc.expected.InterfaceTemplate.DefinedTemplates() {
			t.Errorf("Expected: %+v, Got: %+v", tc.expected, *result)
		}

	}

}

func TestNewStorageTemplate(t *testing.T) {
	tmpl, err := NewStorageTemplate()

	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if tmpl == nil {
		t.Error("Expected template to be created")
	}

	expectedTemplateName := "storage_template"
	if tmpl.Name() != expectedTemplateName {
		t.Errorf("Expected template name to be %s, got: %s", expectedTemplateName, tmpl.Name())
	}

	helloTemplate := tmpl.Lookup("storage_template.tmpl")
	if helloTemplate == nil {
		t.Error("Expected storage_template to be defined")
	}
}

func TestNewInterfaceTemplate(t *testing.T) {
	tmpl, err := NewInterfaceTemplate()

	if err != nil {
		t.Errorf("Expected no error, got: %v", err)
	}

	if tmpl == nil {
		t.Error("Expected template to be created")
	}

	expectedTemplateName := "interface_template"
	if tmpl.Name() != expectedTemplateName {
		t.Errorf("Expected template name to be %s, got: %s", expectedTemplateName, tmpl.Name())
	}

	helloTemplate := tmpl.Lookup("interface_template.tmpl")
	if helloTemplate == nil {
		t.Error("Expected hello_template to be defined")
	}
}
